﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using TheOwl;
using System.IO;
using System.Timers;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Server server = new Server(args);
            server.serve();
        }
    }


    /**
     * simple server for Owl sensor data
     * 
     * Commands are sent as a line of text over a TCP/IP connection, responses are sent back to the client as lines of text.
     * This allows clients to be constructed fro whatever programming language you wish using a TCP/IP client connection.
     * 
     * Two command are handled
     * GET
     * Gets the sensor data each line contains the address, currentAmps, model, accumulatedAmps (semicolon separated)
     * The final line is GET_COMPETE. Therefore if not sensors are found then GET_COMPLETE is all that is returned 
     * when the GET command is issued.
     * 
     * CLOSE
     * Closes the TCP/IP connection to the client.
     * 
     **/
    class Server
    {
    	private const int SOCKET_BUFFER_SIZE=4096;
        private int serverPort;
        private int clientTimeoutMS;
        private bool debugEnabled;
        private TcpListener tcpListener;
        private Thread listenThread;
        private TheOwl.EnergyMonitor myOwl = new TheOwl.EnergyMonitor();
        private ASCIIEncoding encoder = new ASCIIEncoding();
        private bool quiet = false;
        private Mutex logFileMutex = new Mutex();
        private Mutex sensorStringListMutex = new Mutex();
        private String logFile = "owl_server_log.txt";
        private List<string> sensorStringList = new List<string>();
        private StreamWriter sw = null;
        private double version = 1.01985;        
        private bool updateProgressNow;

        /**
         * Parameterless constructor
         **/
        public Server(string[] args)
        {
            //Set default port
            serverPort = 12745;

            bool readServerPort = false;
            bool readLogFile = false;
            foreach( String arg in args )
            {
                if (readServerPort)
                {
                    serverPort = Convert.ToInt32(arg);
                    readServerPort = false;
                }
                else if (readLogFile)
                {
                    logFile = arg;
                    readLogFile = false;
                }
                else if (arg.Equals("-f"))
                {
                    logFile = null;
                    readLogFile = true;
                }
                else if (arg.Equals("-p"))
                {
                    serverPort = -1;
                    readServerPort = true;
                }
                else if( arg.Equals("-q") )
                {
                    quiet=true;
                }
                else if (arg.Equals("-d"))
                {
                    debugEnabled = true;
                }
                else if (arg.Equals("-h"))
                {
                    usage();
                    Environment.Exit(0);
                }
            }
            if (serverPort == -1)
            {
                errorExit("-p argument used but no server port defined.");
                Environment.Exit(-1);
            }
            if (logFile == null)
            {
                errorExit("-f argument used but no log file defined.");
                Environment.Exit(-1);
            }
            //When the client connects to the server it must send some data
            //(a GET command) after 30 seconds or the client will be disconnected.
            clientTimeoutMS = 30000;
            
            //Detect an old format log file and convert it to the new format
            ensureLogFormat();
        }

        /**
         * Detect an old format log file (comma separated lines) and change to 
         * new format (semicolon separated lines). The change was made because
         * in France the lines may include comma's in the numbers which breaks
         * the formatting.
         **/
        public void ensureLogFormat() 
        {
        	if( isCommaSeparatedLog() ) {
        		convertToSemiColonSeparatedLog();
        	}
        }
        
        /**
         * Check to determine if the log file is comma separated.
         * Return true if comma separated false if not.
         **/
        private bool isCommaSeparatedLog() 
        {
        	String line;
        	StreamReader oldLogFileStreamReader=null;
        	//If logfile does not exist don't try and convert
        	if( !File.Exists(logFile) )
        	{
        		return false;
        	}
        	try 
        	{
        		oldLogFileStreamReader = new StreamReader(logFile);
        		//Read first line
        		line = oldLogFileStreamReader.ReadLine();
        		if( line != null && line.Length > 0 )
        		{
		       		string[] elements = line.Split(',');
		       		if( elements.Length == 5 ) {
	    	   			return true;
	       			}
        		}
        		return false;
        	}
        	finally
        	{
        		if( oldLogFileStreamReader != null ) 
        		{
        			oldLogFileStreamReader.Close();
        		}
        	}
        }
        
        /**
         * Convert log file from comma separated format to semicolon separated format
         **/
        private void convertToSemiColonSeparatedLog() 
        {
        	bool conversionSuccess=false;
        	int goodLineCount=0;
        	int badLineCount=0;
        	String line;
        	String newFormatLine;
        	StreamReader oldLogFileStreamReader=null;
        	StreamWriter newLogFileStreamWriter=null;
        	
        	info("Converting the OWLServer log file to new format, please wait...");
        	
        	String newLogFile = logFile+"_semicolon_separated";
        	//If we have a file lying around from an incomplete conversion
        	if( File.Exists(newLogFile) )
        	{
        		//Delete it
        		File.Delete(newLogFile);
        	}
        		
        	try 
        	{
        		line = "";
        		oldLogFileStreamReader = new StreamReader(logFile);
        		newLogFileStreamWriter = new StreamWriter(newLogFile);
        		while( line != null ) 
        		{
        			line = oldLogFileStreamReader.ReadLine();
        			if( line != null )
        			{
        				newFormatLine = getNewFormatLine(line);
        				if( newFormatLine != null )
        				{
	        				newLogFileStreamWriter.WriteLine(newFormatLine);
	        				goodLineCount++;
        				}
        				else
        				{
        					badLineCount++;
        				}
        			}
        		}
        		info("Completed conversion.");
        		info("Converted "+goodLineCount+" lines to new semicolon separated line format.");
        		if( badLineCount > 0 ) 
        		{
        			error("Found "+badLineCount+" incorrectly formated lines in the log file. These will be ignored.");
        		}
        		conversionSuccess=true;
        	}
        	finally
        	{
        		if( oldLogFileStreamReader != null ) 
        		{
        			oldLogFileStreamReader.Close();
        		}
        		if( newLogFileStreamWriter != null ) 
        		{
        			newLogFileStreamWriter.Close();
        		}
        	}
        	//If we successfuully converted a log file
        	if( conversionSuccess ) 
        	{
        		String orgLogFile = logFile+".original";
        		if( File.Exists(orgLogFile) )
        		{
        			File.Delete(orgLogFile);
        		}
        		//Backup the old log
        		File.Move(logFile, logFile+".original");
        		//And replace original with the new logfile
        		File.Move(newLogFile, logFile);
        	}
        }
        
        /**
         * Convert a line of text from comma separated format to semicolon separated format.
         * Returns a string with five lements separated by semicolons or null if the original
         * line does not cotain five comma separated elements.
         **/
        private String getNewFormatLine(String oldFmtLine)
        {
			string[] elements = oldFmtLine.Split(',');
       		if( elements.Length == 5 ) {
				return elements[0]+";"+elements[1]+";"+elements[2]+";"+elements[3]+";"+elements[4];
      		}
			return null;
        }
        	
        /**
         * Exit the program with enough time for the user to read the error text.
         * 
         * If the user did not start the command line program from a command prompt 
         * then the window will disapear when this program exists. Therefore we 
         * provide a short delay to allow the user to read the error mesage. 
         **/
        private void errorExit(string msg)
        {
            error(msg);
            Thread.Sleep(3000);
        }

        /**
         * Display the usage help text
         **/
        private void usage()
        {
            info("-f : Followed by the file to log sensor data (default=C:\\owl_server_log.txt)");
            info("-p : Followed by the server port (default=12745)");
            info("-q : Quiet mode. No text is sent to stdout in this mode.");
            info("-d : Debug on. Send extra debug text to stdout if not in quiet mode.");
            info("-h : Display this help text.");
        }

        /**
         * Constructor that allows the server port, client timeout and debug to be defined
         **/
        public Server(int serverPort, int clientTimeoutMS, bool debugEnabled)
        {
            this.serverPort = serverPort;
            this.clientTimeoutMS = clientTimeoutMS;
            this.debugEnabled = debugEnabled;
        }

        /**
        * Call this (blocking method) to start the server thread and enter a loop reading available sensors
        **/
        public void serve()
        {
            try
            {
                info("Checking for USB Connect device (" + version+ ")");
                int retryCount = 0;
                Boolean deviceInited = false;
                while (true)
                {
                    if ( !deviceInited && initDevice())
                    {
                        deviceInited = true;
                    }
                    if (deviceInited && myOwl.IsAvailable())
                    {
                        break;
                    }
                    if (retryCount >= 20)
                    {
                        if (deviceInited)
                        {
                            errorExit("Failed to find the Electric OWL (USB connect device)");
                        }
                        else
                        {
                            errorExit("Failed to init the Electric OWL (USB connect device)");
                        }
                        return;
                    }
                    Thread.Sleep(1000);
                    retryCount++;
                    info(".");
                }
                //Start the server thread
                //Bind to the socket
                try
                {
                    this.tcpListener = new TcpListener(IPAddress.Any, serverPort);
                    this.listenThread = new Thread(new ThreadStart(ListenForClients));
                    this.listenThread.Start();
                }
                catch (SocketException)
                {
                    //Display 
                    errorExit("It appears that the Electric OWL server is already running as TCP/IP port " + serverPort + " is in use.");
                    Environment.Exit(-1);
                }
                //Block the main thread reading the sensor data
                ReadSensors();
            }
            finally
            {
                myOwl.Free();
            }
        }

        /**
         * Listen for and handle clients
         **/
        private void ListenForClients()
        {
            this.tcpListener.Start();
            info("Listening on TCP/IP port " + serverPort);
            while (true)
            {
                //blocks until a client has connected to the server
                TcpClient client = this.tcpListener.AcceptTcpClient();

                //create a thread to handle communication
                //with connected client
                Thread clientThread = new Thread(new ParameterizedThreadStart(HandleClientComm));
                clientThread.Start(client);
            }
        }

        private void ReadSensors()
        {
            String sensorDataString;

            //Loop to read sensor status
            while (true)
            {
               //Wait to obtain logfile access lock
               logFileMutex.WaitOne();
               try
               {
                   debug("WRITE FILE LOCK OBTAINED");
                   sw = new StreamWriter(logFile, true, new System.Text.ASCIIEncoding());
                   //Empty last data
                   sensorStringList.Clear();
                   //Read the sensor/s
                   Sensor[] sensors = myOwl.Query();
                   //Read the date
                   String dateString = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                   //Log result for each sensor
                   foreach (Sensor s in sensors)
                   {
                       //If sensor is valid
                       if (s.valid)
                       {
                           try
                           {
                               //Obtain lock while we update the sensorDataString
                               sensorStringListMutex.WaitOne();
                               //This string can be read by any client thread to be forwarded to a client
                               sensorDataString = s.addr + ";" + s.currentAmps + ";" + s.model + ";" + s.accumulatedAmps + ";" + dateString;
                           }
                           finally
                           {
                               //Release it now sensorDataString has been updated
                               sensorStringListMutex.ReleaseMutex();
                           }
                           debug("Read sensor Data: " + sensorDataString);
                           sensorStringList.Add(sensorDataString);
                           sw.WriteLine(sensorDataString);
                       }
                   }
                   sw.Close();
               }
               finally
               {
                   //Release logfile access lock
                   logFileMutex.ReleaseMutex();
                   debug("WRITE FILE LOCK RELEASED");
               }
               //Wait until we want to read the sensor again
               Thread.Sleep(3000);
            }
        }

        /**
         * Display an error message on the console
         **/
        public void error(String message)
        {
            if (!this.quiet)
            {
                Console.WriteLine("ERROR: " + message);
            }
        }

        /**
         * Display an info message on the console
         **/
        public void info(String message)
        {
            if (!this.quiet)
            {
                Console.WriteLine("INFO:  " + message);
            }
        }

        public void debug(String message)
        {
            if (this.debugEnabled)
            {
                Console.WriteLine("DEBUG: "+message);
            }
        }

        public bool checkDeviceAvailable()
        {
            if (myOwl.Init())
            {
                Thread.Sleep(100);
                if (myOwl.IsAvailable())
                {
                    return true;
                }
                errorExit("The Electric OWL is currently unavailable");
            }
            else
            {
                errorExit("Failed to initialise the Electric OWL");
            }
            return false;
        }

        /**
         * Initialize the OWL USB connect device
         **/
        private bool initDevice()
        {
            Boolean inited = false;
            if (myOwl.Init())
            {
                //A delay of at least 100 ms is required or a 
                //subsequent call to myOwl.isAvailable may fail
                Thread.Sleep(100);
                inited = true;
            }
            return inited;
        }

        /**
         * Handle client connection
         **/
        private void HandleClientComm(object client)
        {
            TcpClient tcpClient = (TcpClient)client;
            
            NetworkStream clientStream = tcpClient.GetStream();
            //Disconnect the client if we get no get command for 30 seconds
            clientStream.ReadTimeout = clientTimeoutMS;
            byte[] message = new byte[SOCKET_BUFFER_SIZE];
            int bytesRead;
            String rxMessage;

            try
            {
                info("Client connected");

                if (myOwl.IsAvailable())
                {
                    //Loop to read sensor status
                    bool running = true;
                    while (running)
                    {
                        bytesRead = 0;

                        //blocks until a client sends a message
                        bytesRead = clientStream.Read(message, 0, SOCKET_BUFFER_SIZE);

                        if (!myOwl.IsAvailable())
                        {
                            String m="USB_CONNECT_UNAVAILABLE";
                            info(m);
                            sendInfo(m, clientStream);
                            continue;
                        }
                        
                        if (bytesRead == 0)
                        {
                            //the client has disconnected from the server
                            break;
                        }
                        encoder.GetString(message, 0, bytesRead);

                        //message has successfully been received
                        rxMessage = encoder.GetString(message, 0, bytesRead);
                        if (rxMessage.Length > 0)
                        {
                            debug("RX: " + rxMessage.Substring(0, rxMessage.Length - 1));
                            //If client wants the last sensor data
                            if (rxMessage.StartsWith("GET"))
                            {
                                //Wait to obtain sensorStringList access lock.
                                try
                                {
                                    sensorStringListMutex.WaitOne();
                                    if (sensorStringList.Capacity > 0)
                                    {	
                                        foreach (String sensorDataString in sensorStringList)
                                        {
                                            sendInfo("SENSOR_DATA=" + sensorDataString, clientStream);
                                        }
                                    }
                                }
                                finally
                                {
                                    //Release logfile access lock
                                    sensorStringListMutex.ReleaseMutex();
                                    sendInfo("GET_COMPLETE", clientStream);
                                }
                            }
                            //If the client wants all the log data
                            else if (rxMessage.StartsWith("LOG_GET"))
                            {
                                getLog(rxMessage, clientStream, tcpClient);
                            }
                            //Erase log if required
                            else if (rxMessage.StartsWith("ERASELOG"))
                            {
                                try
                                {
                                    //delete the logfile
                                    File.Delete(logFile);
                                    debug("LOGFILE: " + logFile);
                                    sendInfo("ERASELOG_COMPLETE", clientStream);
                                }
                                catch (Exception e)
                                {
                                    sendError(e.ToString(), clientStream);
                                }
                             }
                            //Get log file size
                            else if (rxMessage.StartsWith("LOGSIZE_GET"))
                            {
                                if (File.Exists(logFile))
                                {
                                    FileInfo f = new FileInfo(logFile);
                                    sendInfo("LOGSIZE=" + f.Length, clientStream);
                                }
                                else
                                {
                                    sendInfo("LOGSIZE=0", clientStream);
                                }
                            }
                            else if (rxMessage.StartsWith("CLOSE"))
                            {
                                running=false;
                            }
                            //If we want to init the USB connect device
                            else if (rxMessage.StartsWith("INIT"))
                            {
                                if( initDevice() ) {
                                    sendInfo("INIT_SUCCESS", clientStream);
                                }
                                else{
                                    sendInfo("INIT_FAIL", clientStream);
                                }
                            }
                        }
                    }
                }
                else
                {
                    sendError("Failed to find the Electric OWL (USB Connect receiver)", clientStream);
                }

                //Close the connection to the client
                tcpClient.Close();
            }
            catch (Exception e)
            {
                info("Client disconnected");
                debug(e.Message);
            }
            finally
            {
                try
                {
                    tcpClient.Close();
                }
                catch (Exception) { }
            }
        }

        private void updateProgress(object sender, ElapsedEventArgs e)
        {
        	updateProgressNow=true;
        }
        
        /**
         * Return the contents of the log file to the client
         **/
        private void getLog(String rxMessage, NetworkStream clientStream, TcpClient tcpClient)
        {
            string[] 		elems;
            DateTime 		lastLogDateTime;
            TimeSpan 		dateTimeDiff;
            String   		getLogPeriod="";
            Boolean  		sendLine;
            int      		sensorAddress, model;
            double   		currentAmps, maxCurrentAmps, accumulatedAmps, maxAccumulatedAmps;
            String   		dateTimeString;
            Boolean  		readLastLogDateTime = false;
            Boolean  		readDatetimeDiff = false;
            StreamReader 	reader=null;
            DateTime 		startDate = new DateTime();
            DateTime 		stopDate = startDate;
            bool 			datesFound = false;
            int 			requiredSensorAddress=0;
	        double 			readByteCount;
        	double 			logfileSize;
        	System.Timers.Timer progressTimer;

            dateTimeDiff = startDate.Subtract(startDate);
            lastLogDateTime = startDate;
            readByteCount = 0;
            //If the start/stop dates and time period have been passed
            elems = rxMessage.Split(';');
            if (elems.Length == 5)
            {
                startDate = Convert.ToDateTime(elems[1]);
                stopDate   = Convert.ToDateTime(elems[2]);
                //This element determines how often we should report the values
                //from the log file. It may be
                //SECONDS
                //MINUTES
                //HOURS
                //DAYS
                //WEEKS
                getLogPeriod = elems[3];
                requiredSensorAddress = Convert.ToInt32(elems[4]);
                info("Checking for data on sensor address = " + requiredSensorAddress);
                datesFound = true;
            }
            maxCurrentAmps = 0;
            maxAccumulatedAmps = 0;
            //Grab time so that debugging can display the time to read the log
            DateTime t1 = DateTime.Now;
            //Update progress every second
            updateProgressNow=false;
            progressTimer = new System.Timers.Timer(1000); 
        	progressTimer.Elapsed += new ElapsedEventHandler(this.updateProgress);
        	progressTimer.Enabled = true;            // Enable it
            try
            {
                //Wait to obtain logfile access lock
                logFileMutex.WaitOne();
                debug("READ LOG LOCK OBTAINED, datesFound=" + datesFound + ", startDate=" + startDate + ", stopDate=" + stopDate);
                DateTime startTime = DateTime.Now;
                reader = new StreamReader(logFile);
                string line;
                logfileSize = new FileInfo(logFile).Length;
                while ((line = reader.ReadLine()) != null)
                {
	                //Add the number of bytes just read to the read byte count
	                readByteCount+=line.Length+2;
	                //If its time to update the client as to how far we are through the task of reading the 
	                //log file.
	                if( updateProgressNow )
	                {
	                	double percentage=(readByteCount/logfileSize)*100;
	                	sendInfo("PERCENTAGE=" + (int)percentage, clientStream);
						info("LOG_GET PERCENTAGE COMPLETE = " + (int)percentage);
	    	            updateProgressNow = false;      	
	                }
                    if (datesFound)
                    {
                        //if the start date is after the stop date
                        if (DateTime.Compare(startDate, stopDate) <= 0)
                        {
                            //We need to convert the data in the log file line 
                            elems = line.Split(';');
                            if (elems.Length == 5)
                            {
                                try
                                {
                                    dateTimeString = elems[4];
                                    //Convert date string to date
                                    DateTime logDate = Convert.ToDateTime(dateTimeString);
                                    //If the date from the log file is one the client is interested in
                                    if (DateTime.Compare(logDate, startDate) >= 0 && DateTime.Compare(logDate, stopDate) <= 0)
                                    {
                                        //Convert the rest of the line elements to values
                                        sensorAddress = Convert.ToInt32(elems[0]);
                                        //If this is the sensor were interested in 
                                        if (requiredSensorAddress == sensorAddress)
                                        {
	                                        currentAmps = Convert.ToDouble(elems[1]);
	                                        model = Convert.ToInt32(elems[2]);
	                                        accumulatedAmps = Convert.ToDouble(elems[3]);
	
	                                        //If we have a new max amps
	                                        if (currentAmps > maxCurrentAmps)
	                                        {
	                                            maxCurrentAmps = currentAmps;
	                                        }
	
	                                        //If we have a new max accumulated amps
	                                        if (accumulatedAmps > maxAccumulatedAmps)
	                                        {
	                                            maxAccumulatedAmps = accumulatedAmps;
	                                        }
	
	                                        //If we have already read the log file dateTime
	                                        if (readLastLogDateTime)
	                                        {
	                                            //Calc the difference
	                                            dateTimeDiff = logDate.Subtract(lastLogDateTime);
	                                            readDatetimeDiff = true;
	                                        }
	                                        sendLine = false;
	                                        //If this is the first line 
	                                        if ( !readLastLogDateTime )
	                                        {
	                                            //send it
	                                            sendLine = true;
	                                        }
	                                        //If we are interested in every second (poll period every 3 seconds) and at least a second 
	                                        //has passed
	                                        else if (getLogPeriod.StartsWith("SECONDS") && readDatetimeDiff && dateTimeDiff.TotalSeconds >= 1.0)
	                                        {
	                                            sendLine = true;
	                                        }
	                                        //If we are interested in every minute and at least a minute has passed.
	                                        else if (getLogPeriod.StartsWith("MINUTES") && readDatetimeDiff && dateTimeDiff.TotalMinutes >= 1.0)
	                                        {
	                                            sendLine = true;
	                                        }
	                                        else if (getLogPeriod.StartsWith("HOURS") && readDatetimeDiff && dateTimeDiff.TotalHours >= 1.0)
	                                        {
	                                            sendLine = true;
	                                        }
	                                        else if (getLogPeriod.StartsWith("DAYS") && readDatetimeDiff && dateTimeDiff.TotalDays >= 1.0)
	                                        {
	                                            sendLine = true;
	                                        }
	                                        else if (getLogPeriod.StartsWith("WEEKS") && readDatetimeDiff && dateTimeDiff.TotalDays >= 7.0)
	                                        {
	                                            sendLine = true;
	                                        }
	                                        if (sendLine)
	                                        {
	                                            sendInfo("SENSOR_DATA=" + sensorAddress + ";" + maxCurrentAmps + ";" + model + ";" + maxAccumulatedAmps + ";" + dateTimeString, clientStream);
	                                            //Reset the max values
	                                            maxCurrentAmps = 0;
	                                            maxAccumulatedAmps = 0;
	                                            //Grab the date we sent the lo file data
	                                            lastLogDateTime = logDate;
	                                            //Set the flag to indicate that we read the log DateTime
	                                            readLastLogDateTime = true;
	                                        }
                                        }
                                    }
                                }
                                catch (FormatException)
                                {
                                    //Ignore badly formatted log file lines
                                }
                                catch (OverflowException)
                                {
                                    //Ignore badly formatted log file lines
                                }
                            }
                        }
                    }
                    //If no dates were requested then we send the whole log to the client
                    else
                    {
                        sendInfo("SENSOR_DATA=" + line, clientStream);
                    }
                    //Check for a cancel message from the Electric OWL client
                    if( userCancelGetLog(clientStream, tcpClient) )
                    {
                    	info("Received cancel LOG_GET command");
                    	break;
                    }
                }
            }
            finally
            {
            	progressTimer.Enabled = false;
            	progressTimer.Close();
            	progressTimer.Dispose();
            	progressTimer = null;
                if( reader != null )
                {
                    try
                    {
                        reader.Close();
                    }
                    catch (Exception) { }
                }
                //Release logfile access lock
                logFileMutex.ReleaseMutex();
                DateTime t2 = DateTime.Now;
                TimeSpan duration = t2 - t1;
                debug("READ LOCK RELEASED, MS to execute = " + duration.TotalMilliseconds);
                sendInfo("LOG_GET_COMPLETE", clientStream);
            }
        }
        
        /**
         * Check that the client has not cancelled the log get
         **/
        private bool userCancelGetLog(NetworkStream clientStream, TcpClient tcpClient)
        {
        	int cancelMessageBytesRead;
	    	byte[] cancelMessage = new byte[SOCKET_BUFFER_SIZE];
	    	String cancelMessageString;
	    	 
			//Check for a cancel message from the Electric OWL client
            if( tcpClient.Available > 0 ) {
            	//Read the message
                cancelMessageBytesRead = clientStream.Read(cancelMessage, 0, SOCKET_BUFFER_SIZE);
                //If we received some data
                if( cancelMessageBytesRead > 0 ) 
                {
                		//Extract message
                		cancelMessageString = encoder.GetString(cancelMessage, 0, cancelMessageBytesRead);
                		//If the client wishes to cancel the GET command
                		if( cancelMessageString.StartsWith("CANCEL") ) 
                		{
                			return true;
                		}
                }
            }
			return false;
        }
        
        /**
         * Send an info message to a client
         **/
        private void sendInfo(String message, NetworkStream clientStream)
        {
            send("INFO:  " + message, clientStream);
        }

        /**
         * Send an error message to a client
         **/
        private void sendError(String message, NetworkStream clientStream)
        {
            send("ERROR: " + message, clientStream);
        }

        /**
         * Send a message to a client
         **/
        private void send(String message, NetworkStream clientStream)
        {
            debug("TX: " + message);
            message = message.Replace('.',',');
            byte[] bytes = encoder.GetBytes(message + "\n");
            clientStream.Write(bytes, 0, bytes.Length);
        }
    }



}
