Windows service to output eowl energy meter data over socket. Is based on Paul Austen's work here: https://sourceforge.net/projects/electricowl/

Primarily created to experiment with .NET and solve a use case running EOWL on a Windows home server.
